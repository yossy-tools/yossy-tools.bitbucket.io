/**
 * 🍤 The getElementById shortcut.
 * \ Akane-chan ebifry power! /
 * @param {string} _id
 */
const ebi = _id => document.getElementById(_id);

/**
 * 🍤 The getElementsByClassName shortcut with convert array.
 * @param {string} _name
 */
const ebc = _name => Array.from(document.getElementsByClassName(_name));

/**
 * Generate and deploy image.
 * @param {HTMLElement} _node source area
 * @param {HTMLElement} _area deploy area
 */
const generate_card = (_node, _area) => {
    domtoimage.toPng(_node).then(data => {
        const image = document.createElement('img');
        image.src = data;
        _area.appendChild(image);
    });
}

/**
 * Ignore Enter keyboard event.
 * @param {Event} _event key event
 * @return {boolean} always false
 */
const ignore_enter = _event => {
    if (_event.keyCode == 13 && !_event.shiftKey) {
        _event.preventDefault();
        return false;
    }
}

/**
 * Load font from google fonts.
 * @param {string} _name font name
 */
const load_font = _name => {
    const font = document.createElement('link');
    font.setAttribute('rel', 'stylesheet')
    font.setAttribute('crossorigin', 'anonymous')
    font.setAttribute('type', 'text/css')
    font.setAttribute('href', `https://fonts.googleapis.com/css2?family=${_name}`)
    document.head.appendChild(font);
}

/**
 * load font button handler.
 * @param {HTMLElement} _target
 * @param {string[]} _font_list
 */
const load_font_handle = (_target, _font_list) => {
    if (!_target.classList.contains('loaded')) {
        _font_list.forEach(font => load_font(font));
        _target.classList.add('loaded');
    }
}

ebi('card-data').childNodes.forEach(_node => _node.addEventListener('keydown', ignore_enter));
ebi('card-memo').addEventListener('keydown', ignore_enter);
ebi('button-chinese').addEventListener('click', _event => load_font_handle(_event.target, ['Noto+Sans+HK:wght@700&display=swap', 'Noto+Sans+SC:wght@700&display=swap', 'Noto+Sans+TC:wght@700&display=swap']));
ebi('button-japanese').addEventListener('click', _event => load_font_handle(_event.target, ['Noto+Sans+JP:wght@700&display=swap']));
ebi('button-korean').addEventListener('click', _event => load_font_handle(_event.target, ['Noto+Sans+KR:wght@700&display=swap']));
ebi('button-create').addEventListener('click', () => generate_card(ebi('wrapper'), ebi('result')));
ebc('editable').forEach(_element => _element.addEventListener('blur', _event => twemoji.parse(_event.target)));
document.addEventListener('DOMContentLoaded', () => twemoji.parse(document));
